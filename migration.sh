#!/usr/bin/env bash

function help() {
    echo "migration.sh [-a, --apply] [-p, --password]"
}

if [ -f ./setenv.sh ]
then
    . ./setenv.sh
fi

export CLASSPATH="target/classes:src/main/resources"

currentDir=`pwd -P`;
name=`basename $currentDir`

for jar in `ls target/$name/WEB-INF/lib/*.jar`
do
    export CLASSPATH="$jar:$CLASSPATH"
done

APPLY=false
PASSWORD=false
PASS=''

eval set -- "$@ --"

while true ;
 do
    case "$1" in
        -a | --apply )
          APPLY=true
          shift;
          ;;
        -p | --password )
          PASSWORD=true
          PASS=$2
          shift 2;
          ;;
        -- )
          shift
          break
          ;;
        *)
          help;
          exit 1
          ;;
    esac
done

if [ $PASSWORD = true ]
then
    java -cp $CLASSPATH com.shinitech.djammadev.services.tools.STUtil hash $PASS
    exit 0;
fi

echo "Migration in progress"
java -cp $CLASSPATH com.shinitech.djammadev.migration.MainDbMigration
if [ $APPLY = true ]
then
    echo "Apply Migration in progress"
    java -cp $CLASSPATH com.shinitech.djammadev.migration.ApplyBdMigration
fi
echo "Done."