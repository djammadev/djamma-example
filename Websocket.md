# Websocket

WebSocket is a computer communications protocol, providing full-duplex communication channels over a single TCP connection.
The WebSocket protocol was standardized by the IETF as RFC 6455 in 2011, and the WebSocket API in Web IDL is being standardized by the W3C.

WebSocket is distinct from HTTP. Both protocols are located at layer 7 in the OSI model and depend on TCP at layer 4.
Although they are different, [RFC 6455](https://tools.ietf.org/html/rfc6455) states that WebSocket "is designed to work over HTTP ports 80 and 443 as well as to support HTTP proxies and intermediaries," thus making it compatible with the HTTP protocol.
To achieve compatibility, the WebSocket handshake uses the HTTP Upgrade header[1] to change from the HTTP protocol to the WebSocket protocol.
 

## Use case

Imagine we need to implement a live chat between A and B. When A sends a message to B, B should see the message without refreshing he's web page.
Websocket is a good solution to implement the feature.

## Implementation.

To implement a websocket server with our REST application, we need to create a new communication channel in addition to the http server.

In this framework we use [Webbit - A Java event based WebSocket and HTTP server](https://github.com/webbit/webbit) it is very simple to use.

Maven dependency
```xml
<dependency>
    <groupId>org.webbitserver</groupId>
    <artifactId>webbit</artifactId>
    <version>${webbit.version}</version>
</dependency>
```

Run Server
```java
class Main {
    public static void main(String[] args){
        WebServer webServer = WebServers.createWebServer(8080)
                        .add("/ws", new HelloWebSockets()) // path to web content
                        .start()
                        .get();
    }
}
```

Example of handler
```java
public class HelloWebSockets extends BaseWebSocketHandler {
    private int connectionCount;

    public void onOpen(WebSocketConnection connection) {
        connection.send("Hello! There are " + connectionCount + " other connections active");
        connectionCount++;
    }

    public void onClose(WebSocketConnection connection) {
        connectionCount--;
    }

    public void onMessage(WebSocketConnection connection, String message) {
        connection.send(message.toUpperCase()); // echo back message in upper case
    }
}
```

## Integration in Djamma Dev

* In your application entry point register the websocket loader.
```java
public class MyApplication extends STApplicationBase {

    public MyApplication() {
        register(WebsocketContainerLifecycleListener.class);
        // ... register more elements
    }
}
```

* Create your workers to load in websocket

```java
public class WebsocketWorker extends BaseWebsocketWorker {
    public WebsocketWorker(Config application) {
        super(application, "/ws");
    }

    @Override
    public void onOpen(WebSocketConnection connection) throws Exception {
        connection.send("Welcome !");
    }

    @Override
    public void onMessage(WebSocketConnection connection, String data) throws Throwable {
        // handle message...
    }

    @Override
    public void onClose(WebSocketConnection connection) throws Exception {
        LOGGER.info("Closing connection... " + connection);
    }

    protected void addConnection(String topic, WebSocketConnection connection) {
        connections.computeIfAbsent(topic, k -> new ArrayList<>()).add(connection);
    }

    public void send(String topic, String msg) {
        connections.computeIfAbsent(topic, k -> new ArrayList<>()).forEach(c -> c.send(msg));
    }

}
```

* Update the `application.conf` file to add workers

```hocon
// Required for
websocket {
    port = 9098
    port = ${?WEBSOCKET_PORT}
    workers = [
        {
            path = "/ws"
            worker = "com.shinitech.djammadev.websocket.workers.WebsocketWorker"
        }
    ]
    workers = ${?WEBSOCKET_WORKERS}
}
```

## Usage
In Resources we can access to WebsocketWorkers through `WebsocketContext.INSTANCE`.

Each worker is added into `WebsocketContext.INSTANCE`  
Example : 
If we want to send a message to the topic `all` in channel `/ws` we can use the code bellow.  

```java
public class MyResource extends BasicResource {
    public Response notifyAllChannel() {
        BaseWebsocketWorker worker = websocketContext.getValue("/ws", BaseWebsocketWorker.class);
        if (worker != null) {
            worker.send("all", Json.pretty(bean));
        }
    }
}
```

## Javascript implementation

```html
<script>
    function showMessage(text) {
        document.getElementById('ws-message').innerHTML = text;
    }

    var sockect = new WebSocket('ws://localhost:9091/ws');
    showMessage('Connecting...');
    sockect.onopen = function () {
        showMessage('Connected!');
    };
    sockect.onclose = function () {
        showMessage('Lost connection');
    };
    sockect.onmessage = function (msg) {
        showMessage(msg.data);
    };

    function handleLogin(request) {
        let token = request.getResponseHeader('x-auth-token');
        let value = {
            token: token,
            topic: 'all'
        };
        sockect.send(JSON.stringify(value));
    }
</script>
```

## Resources

* [Wikipedia](https://en.wikipedia.org/wiki/WebSocket)

