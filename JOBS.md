# Jobs
Combine [Heroku](https://dashboard.heroku.com/), [DjammaRestfull](/) and `crontab` to schedule task.

Use [DjammaRestfull](/) framework to create a quick application and schedule job with `crontab` to execute tasks.

## DjammaRestfull Application

See [Quick Start with DjammaRestfull](README.md)

Add `JobRunnerResource` into your application.

### Entry Point
````java
@ApplicationPath("/")
public class MyApplication extends STApplicationBase {
    public MyApplication() {
        // More lines
        register(JobRunnerResource.class);
    }
}
````

### Configuration

Add into `application.conf`

```hocon
triggers += {
  name: "my-job-every-hour",
  className: "com.shinitech.djammadev.myapp.jobs.MyDailyTaskJob"
}
```

## Heroku Deployment

Use [heroku Deployment](https://devcenter.heroku.com/articles/github-integration) for different ways of deployment.


## Crontab

Call webservice from crontab

```bash
0 */1 * * * curl -X POST -H 'Authorization: Bearer Token' -H 'Content-Type: application/json' -d '{"name": "my-job-every-hour"}' https://myapp.herokuapp.com/api/jobs/v1/async
```

