#!/bin/bash

if [ $# = 0 ]
then
    help;
    exit 1
fi

function exitIfError() {
    if [ ! $1 -eq 0 ] ;
    then
        echo $2;
        exit $1 ;
    fi
    return 0;
}

function help() {
    echo "jersey start|stop|restart|reload [OPTIONS]"
    echo "       -a, --app : application name"
    echo "       -p, --port : running port"
    echo "       -w, --war : application war, if different from application name"
    echo "       -d, --baseDir : application base dir"
    echo "       -b, --build : Build project."
    echo "       -r, --runner : war runner example : target/dependency/webapp-runner.jar."
    echo "       -e, --env : environment file to run (setenv.sh)."
    echo "       -h, --help : print this message."
    echo "       -D, --debug : print params."
    echo "       -t, --tunnel : create a ssh tunnel."
    echo "       -H, --ssh-host : ssh server host."
    echo "       -u, --ssh-user : ssh server user."
    echo "       -m, --migration : Run migration script."
    echo "       -j, --jar : Run jar file."
}

function debug() {
    echo "====== PARAMS ======"
    echo "BASE_DIR = $BASE_DIR"
    echo "APPNAME  = $APPNAME"
    echo "PORT     = $PORT"
    echo "WAR      = $WAR"
    echo "RUNNER   = $RUNNER"
    echo "BUILD    = $BUILD"
    echo "DEBUG    = $DEBUG"
    echo "ENV      = $ENV"
}

USER_HOME=$HOME
cmd=$1;
shift
BUILD="no"
DEBUG="no"
TUNNEL="no"
MIGRATION="no"
SSH_HOST="146.185.151.93"
SSH_USER=`whoami`
JAR="no"
NPM="no"

SHORT="p:a:w:d:r:e:bhDtu:H:mjn"
LONG="--port:,--app:,--war:,--baseDir:,--runner,--build,--env,--help,--debug,--tunnel,--ssh-host,--ssh-user,--migration,--jar,--npm"
OPTS=$(getopt --options $SHORT --long $LONG --name "$0" -- "$@")

eval set -- "$@ --"

exitIfError $? "Problem on init."

while true ;
 do
    case "$1" in
        -p | --port )
          PORT=$2
          shift 2
          ;;
        -a | --app )
          APPNAME="$2"
          shift 2
          ;;
        -d | --baseDir )
          BASE_DIR="$2"
          shift 2
          ;;
        -b | --build )
          BUILD="yes"
          shift
          ;;
        -w | --war )
          WAR="$2"
          shift 2
          ;;
        -r | --runner )
          RUNNER="$2"
          shift 2
          ;;
        -e | --env )
          ENV="$2"
          shift 2
          ;;
        -H | --ssh-host )
          SSH_HOST="$2"
          shift 2
          ;;
        -u | --ssh-user )
          SSH_USER="$2"
          shift 2
          ;;
        -h | --help )
          help;
          exit 1;
          ;;
        -t | --tunnel )
          TUNNEL="yes";
          shift
          ;;
        -m | --migration )
          MIGRATION="yes";
          shift
          ;;
        -j | --jar )
          JAR="yes";
          shift
          ;;
        -n | --npm )
          NPM="yes";
          shift
          ;;
        -D | --debug )
          DEBUG="yes";
          shift
          ;;
        -- )
          shift
          break
          ;;
        *)
          help;
          exit 1
          ;;
    esac
done

currentDir=`pwd -P`

if [ -z $BASE_DIR ] && [ -z $APPNAME ]
then
    BASE_DIR=`dirname $currentDir`
elif [ -z $BASE_DIR ]
then
    BASE_DIR=$currentDir
fi

if [ -z $APPNAME ]
then
    APPNAME=`basename $currentDir`
fi

APP_DIR=$BASE_DIR/$APPNAME
TARGET_DIR=$APP_DIR/target
PORT_FILE="PORT"
RUNNING="RUNNING"
TUNNELLING="TUNNELLING"
PORT_VAR=PORT_$APPNAME
APP_PORT=${!PORT_VAR}

if [ -z $WAR ]
then
    WAR="$APPNAME"
fi

if [ -z $ENV ]
then
    ENV=$APP_DIR/setenv.sh
fi

if [ -r $ENV ]
then
    . $ENV
else
    echo "$ENV is not readable..."
fi

if [ -z $RUNNER ]
then
    RUNNER="$TARGET_DIR/dependency/webapp-runner.jar"
fi

if [ -z $PORT ] && [ -f $APP_DIR/$PORT_FILE ]
then
    PORT=`cat $APP_DIR/$PORT_FILE`
fi

if [ "$DEBUG" = "yes" ]
then
    debug
fi

function setRunnerArgs() {
    if [ -z $WEB_RUNNER_ARGS ]
    then
        WEB_RUNNER_ARGS="--temp-directory $HOME/target/$APPNAME --port $PORT"
    else
        WEB_RUNNER_ARGS="--port $PORT $WEB_RUNNER_ARGS"
    fi
}

function build() {
    cd $APP_DIR;
    mvn clean install -DskipTests
    exitIfError $? "Maven failed."
    cd -;
}

function createTunnel() {
    ssh -f -nN -R $PORT:localhost:$PORT $SSH_USER@$SSH_HOST
    pgrep -f 'ssh.*-f' > $APP_DIR/$TUNNELLING
    exitIfError $? "Tunnel creation failed."
}

function migration() {
    CLASSPATH="$TARGET_DIR/classes:src/main/resources"

    for jar in `ls $TARGET_DIR/$WAR/WEB-INF/lib/*.jar`
    do
    CLASSPATH="$jar:$CLASSPATH"
    done

    echo "Migration in progress"
    java -cp $CLASSPATH com.shinitech.djammadev.migration.MainDbMigration
    exitIfError $? "Migration failed."
    echo "Apply Migration in progress"
    java -cp $CLASSPATH com.shinitech.djammadev.migration.ApplyBdMigration
    exitIfError $? "Migration application failed."
    echo "Done."
}

function runJar() {
    java -cp $CLASS_PATH $MAIN_CLASS $JAR_ARGS
}

function runNpm() {
    if [ -z $NMP_ARGS ]
    then
        NMP_ARGS="start"
    fi
    npm run $NMP_ARGS 3>&1 4>&2 > $APP_DIR/$APPNAME.log 2>&1 &
    echo $! > $APP_DIR/$RUNNING
}

function do_start() {
    if [ "$JAR" = "yes" ]
    then
        runJar
    elif [ "$NPM" = "yes" ]
    then
        runNpm
    else
        if [ "$BUILD" = "yes" ]
        then
            echo "Building $APPNAME"
            build;
        fi
        if [ "$MIGRATION" = "yes" ]
        then
            echo "Migration $APPNAME"
            migration -a;
        fi
        if [ -f $APP_DIR/$RUNNING ]
        then
            echo "File $APP_DIR/RUNNING exists"
            echo "Please remove it to run server on the port $PORT"
            return 1;
        elif [ -z "$PORT" ]
         then
            echo "You must specify a PORT file into $APP_DIR/$PORT_FILE"
            help;
            return 1
         else
            RUNNABLE=$TARGET_DIR/current.war
            if [ -f $RUNNABLE ]
            then
                echo "Using alias current.war"
            else
                echo "Using app name $WAR.war"
                RUNNABLE=$TARGET_DIR/${WAR}.war
            fi
            setRunnerArgs;
            echo "java $JAVA_ARGS -jar $RUNNER $WEB_RUNNER_ARGS $RUNNABLE 3>&1 4>&2 > $APP_DIR/$APPNAME.log 2>&1 &"
            java $JAVA_ARGS -jar $RUNNER $WEB_RUNNER_ARGS $RUNNABLE 3>&1 4>&2 > $APP_DIR/$APPNAME.log 2>&1 &
            echo $! > $APP_DIR/$RUNNING
            echo "$APPNAME Running..."
        fi
    fi
    if [ $TUNNEL = "yes" ]
    then
        echo "Creating ssh tunnel with $SSH_USER@$SSH_HOST on port $PORT"
        createTunnel
    fi
    return 0;
}

function do_stop() {
    killProcess $APP_DIR/$RUNNING "$APPNAME is not running";
    killProcess $APP_DIR/$TUNNELLING "No tunnel is created";
    return 0;
}

function killProcess() {
    if [ -f $1 ]
    then
        echo "kill `cat $1`"
        kill -9 `cat $1`
        echo "rm -rf $1"
        rm -rf $1
    else
        echo "$2"
    fi
}

function do_restart() {
    do_stop;
    echo "Waiting for server to be graceful stopped..."
    sleep 3;
    do_start;
    return 0;
}

function do_status() {
    if [ -f $APP_DIR/$RUNNING ]
    then
        echo "$APPNAME is running on `cat $APP_DIR/$PORT_FILE` with PID `cat $APP_DIR/$RUNNING`"
    else
        echo "$APPNAME is not running"
    fi
    lsof -i ":$PORT"
    return 0
}

case "$cmd" in
  start|"")
        do_start
        ;;
  restart|reload)
        do_restart
        ;;
  stop)
        do_stop
        ;;
  status)
        do_status
        ;;
  *)
        help;
        exit 3
        ;;
esac
:
