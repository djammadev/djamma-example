package com.djammadev.resources.interview;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.shinitech.djammadev.resources.BasicResource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-16
 */
@Api("Hello World")
@Path("/hello")
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML, MediaType.TEXT_PLAIN})
public class HelloResource extends BasicResource {

    @GET
    @ApiOperation("Say Hello to User.")
    public Response sayHello(@QueryParam("name") String name) {
        return Response.ok().entity(String.format("Hello %s", name)).type(MediaType.TEXT_PLAIN).build();
    }
}