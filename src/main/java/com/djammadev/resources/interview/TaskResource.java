package com.djammadev.resources.interview;

import com.djammadev.models.interview.Task;
import com.shinitech.djammadev.interceptor.SecuredSession;
import com.shinitech.djammadev.models.finder.FinderBase;
import com.shinitech.djammadev.resources.AbstractSharableResource;
import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-16
 */
@Api("Tasks")
@Path("/task")
@Consumes({MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON, MediaType.MULTIPART_FORM_DATA})
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
@SecuredSession
public class TaskResource extends AbstractSharableResource<Task> {

    public TaskResource() {
        this.disableCache = true;
    }

    @Override
    protected FinderBase<Task> find() {
        return new FinderBase<>(Task.class);
    }

    @Override
    protected String getSubKey() {
        return "task";
    }
}