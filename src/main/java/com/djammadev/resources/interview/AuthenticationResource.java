package com.djammadev.resources.interview;

import com.auth0.jwt.JWTCreator;
import com.avaje.ebean.Finder;
import com.djammadev.models.interview.User;
import com.shinitech.djammadev.resources.AbstractAuthenticationResource;
import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.*;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-20
 */
@Path("/auth")
@Api("Authentication")
@Produces({APPLICATION_JSON, TEXT_PLAIN})
@Consumes({APPLICATION_JSON, APPLICATION_FORM_URLENCODED})
public class AuthenticationResource extends AbstractAuthenticationResource<User> {

    @Override
    protected JWTCreator.Builder enrich(JWTCreator.Builder builder, User user) {
        return builder.withArrayClaim("roles", user.roles());
    }

    @Override
    protected Finder<Long, User> find() {
        return new Finder<>(User.class);
    }
}
