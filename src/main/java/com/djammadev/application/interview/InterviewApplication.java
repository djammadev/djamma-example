package com.djammadev.application.interview;

import com.shinitech.djammadev.application.STApplicationBase;

import javax.ws.rs.ApplicationPath;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-16
 */
@ApplicationPath("/")
public class InterviewApplication extends STApplicationBase {

    public InterviewApplication() {
        packages("com.djammadev.resources.interview", "com.shinitech.djammadev.resources.security");
    }
}
