package com.djammadev.websocket.interview;

import com.shinitech.djammadev.websocket.workers.BaseWebsocketWorker;
import com.typesafe.config.Config;
import org.webbitserver.WebSocketConnection;

/**
 * @author Djamma Dev by sissoko
 * @date 20/03/2020
 */
public class TaskWorker extends BaseWebsocketWorker {
    public TaskWorker(Config application, String path) {
        super(application, path);
    }

    @Override
    public void onOpen(WebSocketConnection connection) throws Exception {
        addConnection("tasks", connection);
    }

    @Override
    public void onClose(WebSocketConnection connection) throws Exception {
        removeConnection(connection);
    }
}
