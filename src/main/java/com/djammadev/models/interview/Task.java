package com.djammadev.models.interview;

import javax.persistence.*;

import com.shinitech.djammadev.models.ModelBase;

import java.util.List;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-16
 */
@Entity
@Table(name = "T_TASKS")
public class Task extends ModelBase {
    @Column(name = "TITLE")
    private String title;
    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(mappedBy = "what", cascade = CascadeType.ALL)
    private List<TaskShare> shares;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}