package com.djammadev.models.interview;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.shinitech.djammadev.models.ModelBase;
import com.shinitech.djammadev.models.STUser;
import com.shinitech.djammadev.models.security.Profile;
import com.shinitech.djammadev.models.security.Role;
import com.shinitech.djammadev.models.security.UserProfile;
import com.shinitech.djammadev.models.security.UserRoleView;
import com.shinitech.djammadev.services.provider.JsonIncludeProperties;
import com.shinitech.djammadev.services.provider.STSerializer;

import javax.persistence.*;
import java.util.List;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-20
 */
@Entity
@Table(name = "T_USERS")
@JsonSerialize(using = User.Serializer.class)
public class User extends ModelBase implements STUser {
    @Column(name = "USERNAME", unique = true)
    private String username;
    @Column(name = "PASSWORD")
    private String password;
    @ManyToOne
    @JoinColumn(name = "PROFILE_ID")
    private Profile profile;
    @ManyToOne
    @JoinColumn(name = "ROLE_ID")
    private Role role;

    public User() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserRoleView> getRoles() {
        return UserRoleView.find.where().eq("userId", getId()).findList();
    }

    public List<UserProfile> getProfiles() {
        return UserProfile.find.where().eq("userId", getId()).findList();
    }

    @JsonIncludeProperties({"id", "name"})
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @JsonIncludeProperties({"id", "name"})
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String[] roles() {
        List<UserRoleView> roles = getRoles();
        String[] rolesArray = new String[roles.size()];
        for (int i = 0; i < roles.size(); i++) {
            rolesArray[i] = roles.get(i).getName();
        }
        return rolesArray;
    }

    static class Serializer extends STSerializer<User> {}
}
