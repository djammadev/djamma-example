package com.djammadev.models.interview;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shinitech.djammadev.models.ShareBase;

import javax.persistence.*;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-29
 */
@Entity
@Table(name = "T_TASKS_SHARE", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"WHO_ID", "WHAT_ID"})
})
public class TaskShare extends ShareBase<Task> {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "ACTIVE", columnDefinition = "BIT DEFAULT TRUE")
    private Boolean active;
    @Column(name = "WHO_ID")
    private Long whoId;
    @ManyToOne
    @JoinColumn(name = "WHAT_ID", referencedColumnName = "id")
    private Task what;

    public TaskShare() {
        this.active = true;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public Long getWhoId() {
        return whoId;
    }

    @Override
    public void setWhoId(Long whoId) {
        this.whoId = whoId;
    }

    @Override
    @JsonIgnore
    public Task getWhat() {
        return what;
    }

    @Override
    public void setWhat(Task what) {
        this.what = what;
    }
}
