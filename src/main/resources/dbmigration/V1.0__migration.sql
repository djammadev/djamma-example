-- APPLY CHANGES
CREATE TABLE T_TASKS (
  ID                            BIGINT AUTO_INCREMENT NOT NULL,
  ACTIVE                        BIT DEFAULT TRUE,
  CREATED_BY                    VARCHAR(255) DEFAULT 'UNKNOW',
  MODIFIED_BY                   VARCHAR(255) DEFAULT 'UNKNOW',
  OWNER_ID                      BIGINT,
  TITLE                         VARCHAR(255),
  DESCRIPTION                   VARCHAR(255),
  CREATE_DATE                   DATETIME DEFAULT NOW() NOT NULL,
  MODIFY_DATE                   DATETIME DEFAULT NOW() NOT NULL,
  CONSTRAINT PK_T_TASKS PRIMARY KEY (ID)
);

