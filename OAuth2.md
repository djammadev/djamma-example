# OAuth2 Implementation

In this document we will show how to implement OAuth2 features.  

```md
+--------+                               +---------------+
|        |--(A)- Authorization Request ->|   Resource    |
|        |                               |     Owner     |
|        |<-(B)-- Authorization Grant ---|               |
|        |                               +---------------+
|        |
|        |                               +---------------+
|        |--(C)-- Authorization Grant -->| Authorization |
| Client |                               |     Server    |
|        |<-(D)----- Access Token -------|               |
|        |                               +---------------+
|        |
|        |                               +---------------+
|        |--(E)----- Access Token ------>|    Resource   |
|        |                               |     Server    |
|        |<-(F)--- Protected Resource ---|               |
+--------+                               +---------------+

         Figure 1: General Protocol OAuth2

 A. The application requests authorization to access service resources from the user  
 B. If the user authorized the request, the application receives an authorization grant  
 C. The application requests an access token from the authorization server (API) by presenting authentication of its own identity, and the authorization grant  
 D. If the application identity is authenticated and the authorization grant is valid, the authorization server (API) issues an access token to the application. Authorization is complete.  
 E. The application requests the resource from the resource server (API) and presents the access token for authentication  
 F. If the access token is valid, the resource server (API) serves the resource to the application
```

## Resource Owner : User
The resource owner is the user who authorizes an application to access their account. The application’s access to the user’s account is limited to the “scope” of the authorization granted (e.g. read or write access).

The User is you or me. The authorizer of `Client` to access your resources.

User looks like :

```json
{
    "id": "alpha-numeric",
    "firstName": "John",
    "lastName": "Doe",
    "email": "john.doe@djammadev.com",
    "password": "xxxxxxxxxxxx"
}
```

## Authorization Server : API
The resource server hosts the protected user accounts, and the authorization server verifies the identity of the user then issues access tokens to the application.

From an application developer’s point of view, a service’s API fulfills both the resource and authorization server roles. We will refer to both of these roles combined, as the Service or API role.

Some examples of Authorization Server are `Facebook`, `Twitter`, `Google`, `Linkedin`, ....

![Login Screen](login-screen.png)

These servers keep User resources and grant access to clients.

Example of implementation from Authorization server.

```java
/**
 * @author Djamma Dev by sissoko
 * @date 2019-11-18
 */
@Path("/")
@Api("Authentication")
@Produces({APPLICATION_JSON, TEXT_PLAIN})
@Consumes({APPLICATION_JSON, APPLICATION_FORM_URLENCODED})
public class AuthenticationResource extends BasicResource {

    @POST
    @Path("/token")
    @ApiOperation(value = "Get new Access Token", response = OAuth2AccessTokenResponse.class)
    @ApiResponses({
            @ApiResponse(code = HttpStatus.SC_OK, message = "OK – Success authenticated"),
            @ApiResponse(code = HttpStatus.SC_UNAUTHORIZED, message = "Authentication failed.")
    })
    public Response getAccessToken(OAuth2AccessTokenRequest request) {
        OAuth2AccessTokenResponse response = null;
        if (GRANT_TYPE_CLIENT_CREDENTIALS.equalsIgnoreCase(request.getGrant_type())) {
            response = authWithClientCredentials(request);
        } else if (GRANT_TYPE_PASSWORD.equalsIgnoreCase(request.getGrant_type())) {
            response = authWithClientPassword(request);
        }
        if (response == null) {
            return Response.status(HttpStatus.SC_UNAUTHORIZED).entity(unauthirize("ERROR", "Unauthorized")).build();
        }
        return buildOkResponse(response);
    }
    
    private OAuth2AccessTokenResponse authWithClientCredentials(OAuth2AccessTokenRequest request) {
        User user = User.find.where().eq("username", request.getClient_id())
                .eq("password", request.getClient_secret()).findUnique();
        if (user == null) {
            return null;
        }
        return generateToken(user);
    }
    
    private OAuth2AccessTokenResponse authWithClientPassword(OAuth2AccessTokenRequest request) {
        User user = User.find.where().eq("username", request.getUsername()).findUnique();
        if (user == null || !this.hash.checkPassword(request.getPassword(), user.getPassword())) {
            return null;
        }
        return generateToken(user);
    }
}

/**
 * @author Djamma Dev by sissoko
 * @date 2019-11-19
 */
public class OAuth2AccessTokenRequest {
    private String grant_type;
    private String redirect_uri;
    private String client_id;
    private String client_secret;
    private String username;
    private String password;
}

/**
 * @author Djamma Dev by sissoko
 * @date 2019-11-19
 */
public class OAuth2AccessTokenResponse {
    private String access_token;
    private Integer expires_in;
    private String token_type;
    private String scope;
    private String refresh_token;
}
```

## Client: Application
The client is the application that wants to access the user’s account. Before it may do so, it must be authorized by the user, and the authorization must be validated by the API.

Example : when you want to login into a mobile application with you Facebook account. The server behind the mobile app is the Client.

## Resource Server
The resource Server can be the same as Authorization Server (most of the time). But some time it is different.  
The resource do not care about the authentification mecanism.  The only thing it needs is the access token issued by Authorization Server.

Resource Server is able to understand all access token issued by Authorization Server.

Implementation example:

```java
/**
 * @author Djamma Dev by sissoko
 * @date 2019-12-03
 */
@Path("/v1/projects")
@Api("Product Resource")
@Produces({APPLICATION_JSON, TEXT_PLAIN})
@Consumes({APPLICATION_JSON, APPLICATION_FORM_URLENCODED})
@OAuth2Authorization
public class ProductResource extends BasicResource {
    @Path("/list")
    @ApiOperation(value = "Get Product List.",
            authorizations = {
                    @Authorization(value = "Bearer", scopes = {})
            })
    public Response getProductList() {
        return buildOkResponse(Product.find.findList());
    }
}
```

`curl -X GET -H "Authorization: Bearer ACCESS_TOKEN" https://api.djammadev.com/v1/projects/list`

## Conclusion

The OAuth2 is a strong authorization technique to connect different Server / API.  
If you use many micro services you may implement a variant of OAuth2 to centralize Authentification mecanism.

```md
+-----------+        +-----------+        +-----------+
| Service 1 |        | Service 2 |        | Service 3 |
+-----------+        +-----------+        +-----------+
      |                    |                    |
      |                    |                    |
+-----------------------------------------------------+
|               Authorization Server                  |
+-----------------------------------------------------+
      |                    |                    |
      |                    |                    |
+-----------+        +-----------+        +-----------+
|    User   |        |    User   |        |    User   |
+-----------+        +-----------+        +-----------+
```

## Resources

* [https://www.digitalocean.com/community/tutorials/an-introduction-to-oauth-2](https://www.digitalocean.com/community/tutorials/an-introduction-to-oauth-2)  
* [https://tools.ietf.org/html/rfc6749](https://tools.ietf.org/html/rfc6749)  


