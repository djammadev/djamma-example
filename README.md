# Djamma Dev Framework

## I. Quick Start
In this post we will create a sample REST application using DjammaRestfull framework.  
[https://oss.sonatype.org/content/groups/public/com/shinitech/djammadev/](https://oss.sonatype.org/content/groups/public/com/shinitech/djammadev/)

This framework is based on 2 others frameworks.  
- Jersey (Jaxrs) 
- Ebean (ORM)

### 1. Create a maven project  

`mvn archetype:generate -DarchetypeArtifactId="maven-archetype-webapp" -DarchetypeGroupId="org.apache.maven.archetypes" -DarchetypeVersion="1.4"`

### 2. Add sonatype base repositories  

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.djammadev</groupId>
    <artifactId>djamma-interview</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>war</packaging>

    <name>djamma-interview Maven Webapp</name>
    <!-- FIXME change it to the project's website -->
    <url>http://www.example.com</url>
    <!-- More elements -->
    <repositories>
        <repository>
            <id>snapshots</id>
            <name>DjammaDev internal repository</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </repository>
        <repository>
            <id>staging</id>
            <name>DjammaDev internal repository</name>
            <url>https://oss.sonatype.org/content/groups/staging</url>
        </repository>
        <repository>
            <id>releases</id>
            <name>DjammaDev internal repository</name>
            <url>https://oss.sonatype.org/content/groups/public</url>
        </repository>
    </repositories>
</project>
```

### 3. Add Djamma Core module dependency.


```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.shinitech.djammadev</groupId>
    <artifactId>djamma-interview</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>war</packaging>

    <name>djamma-interview Maven Webapp</name>
    <!-- More elements -->
    <dependencies>
        <dependency>
            <groupId>com.shinitech.djammadev</groupId>
            <artifactId>djamma-core</artifactId>
            <version>1.9-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
    <!-- More elements -->
</project>
```

### 4. Create Application Entry point class inheriting from `STApplicationBase`

```java
package com.djammadev.application.interview;

import com.shinitech.djammadev.application.STApplicationBase;

import javax.ws.rs.ApplicationPath;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-16
 */
@ApplicationPath("/")
public class InterviewApplication extends STApplicationBase {
}
```  

### 5. Update [src/main/webapp/WEB-INF/web.xml](src/main/webapp/WEB-INF/web.xml) to use Application entry point [`com.djammadev.application.interview.InterviewApplication`](src/main/java/com/djammadev/application/interview/InterviewApplication.java)

```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>
  <display-name>Interview Web Application</display-name>
  <servlet>
    <servlet-name>InterviewApplication</servlet-name>
    <servlet-class>org.glassfish.jersey.servlet.ServletContainer</servlet-class>
    <init-param>
      <param-name>javax.ws.rs.Application</param-name>
      <param-value>com.djammadev.application.interview.InterviewApplication</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
  </servlet>
  <servlet-mapping>
    <servlet-name>InterviewApplication</servlet-name>
    <url-pattern>/api/*</url-pattern><!-- All call to /api/* will be forwarded to Application Resources -->
  </servlet-mapping>
</web-app>
```

### 6. Test your application ! Create [src/main/webapp/index.html](src/main/webapp/index.html) if it does not exist. 

```html
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Djamma Dev - Hello world !</title>
</head>
<body>
<h2>Hello World!</h2>
</body>
</html>
```

### 7. Run your Application !

You can use Tomcat or every tools able to run `war` application.

The script bellow is a home made script. It is accessible here [jersey.sh](./jersey.sh).

jersey needs web-runner plugin. this plugin runs a custom tomcat server for your application. Everything is automatic.

```xml
<build>
    <!-- elements -->
    <plugins>
        <plugin>
        <!-- Unzip shared resources -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
            <execution>
                <phase>package</phase>
                <goals>
                    <goal>copy</goal>
                </goals>
                <configuration>
                    <artifactItems>
                        <artifactItem>
                            <groupId>com.github.jsimone</groupId>
                            <artifactId>webapp-runner</artifactId>
                            <version>8.0.30.2</version>
                            <destFileName>webapp-runner.jar</destFileName>
                        </artifactItem>
                    </artifactItems>
                </configuration>
            </execution>
        </executions>
    </plugin>
    </plugins>
</build>
```
`./jersey.sh restart --port 9999 --build`

On the browser at [http://localhost:9999](http://localhost:9999) we can see.  

![preview](hello-world.png).

### 8. Add a resource [com.djammadev.resources.interview.HelloResource](src/main/java/com/djammadev/resources/interview/HelloResource.java) inheriting from `com.shinitech.djammadev.resources.BasicResource`.

`BasicResource` contains common functions used in resources.

We can create a simple Resource class which expose a `GET` method to say Hello {!name} where `{!name}` is a query parameter.

Let's create this resource.

```java
package com.djammadev.resources.interview;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.shinitech.djammadev.resources.BasicResource;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-16
 */
@Path("/hello")
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML, MediaType.TEXT_PLAIN})
public class HelloResource extends BasicResource {
    
    @GET
    public Response sayHello(@QueryParam("name") String name) {
        return Response.ok().entity(String.format("Hello %s", name)).build();
    }
}
```

* Modify `InterviewApplication` to add all resources in package `com.djammadev.resources.interview` 

```java
package com.djammadev.application.interview;

import com.shinitech.djammadev.application.STApplicationBase;

import javax.ws.rs.ApplicationPath;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-16
 */
@ApplicationPath("/")
public class InterviewApplication extends STApplicationBase {

    public InterviewApplication() {
        packages("com.djammadev.resources.interview");
    }
}
```

* Restart the application and go to [http://localhost:9999/api/hello?name=Djamma Dev](http://localhost:9999/api/hello?name=Djamma%20Dev)

`./jersey.sh restart --port 9999 --build`

### 9. Integrate a database (H2).

We will use H2 inMemory database for this example.

- Add `ebean.properties` file into `src/main/resources` 

```properties
ebean.migration.applyPrefix=V
ebean.migration.generate=${EBEAN_MIGRATION_GENERATE}
ebean.migration.run=${EBEAN_MIGRATION_RUN}
ebean.persistBatch=NONE
ebean.persistBatchOnCascade=ALL
#ebean.uuidStoreAsBinary=true
ebean.cacheWarmingDelay=-1
datasource.default=db
# Datasource
datasource.db.username=${DATABASE_USERNAME}
datasource.db.password=${DATABASE_PASSWORD}
datasource.db.databaseUrl=${DATABASE_URL}
datasource.db.databaseDriver=${DATABASE_DRIVER}

ebean.packages=com.djammadev.models.interview
```

- Define Env variables in file `[setenv.sh](./setenv.sh)` this script is executed in `jersey.sh`.
You can change to MySQL database if you want.

```sh
#!/usr/bin/env bash
export DATABASE_USERNAME=rosaot
export DATABASE_PASSWORD=
export DATABASE_DRIVER=org.h2.Driver
export DATABASE_URL=jdbc:h2:mem:myproject;MODE=MYSQL;DB_CLOSE_DELAY=-1
export EBEAN_MIGRATION_GENERATE=true
export EBEAN_MIGRATION_RUN=true
```

### 10. Create an Entity model `Task` with table name `T_TASKS`.  
This table definition is automatically create if `EBEAN_MIGRATION_GENERATE` is true.  
And the table will be created on start up if `EBEAN_MIGRATION_RUN` is true.  

`Task` inherits base class `ModelBase`.  

```java
package com.djammadev.models.interview;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.shinitech.djammadev.models.ModelBase;

@Entity
@Table(name = "T_TASKS")
public class Task extends ModelBase {
    @Column(name = "TITLE")
    private String title;
    @Column(name = "DESCRIPTION")
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
```

- Add ebean enhancer plugin to `pom.xml`  

```xml
<!-- More elements -->
<properties>
    <!-- More elements -->
    <ebean.version>8.1.1</ebean.version>
</properties>
<!-- More elements -->
<plugins>
<!-- More elements -->
    <plugin>
        <groupId>org.avaje.ebeanorm</groupId>
        <artifactId>avaje-ebeanorm-mavenenhancer</artifactId>
        <version>${ebean.version}</version>
        <executions>
            <execution>
                <id>main</id>
                <phase>process-classes</phase>
                <goals>
                    <goal>enhance</goal>
                </goals>
                <configuration>
                    <packages>com.djammadev.models.interview.**</packages>
                    <transformArgs>debug=1;transientInternalFields=true</transformArgs>
                    <classSource>${project.build.outputDirectory}</classSource>
                </configuration>
            </execution>
        </executions>
    </plugin>
    <plugin>
        <groupId>org.avaje.ebean</groupId>
        <artifactId>querybean-maven-plugin</artifactId>
        <version>${ebean.version}</version>
        <executions>
            <!-- enhance code -->
            <execution>
                <id>main</id>
                <phase>process-classes</phase>
                <configuration>
                    <transformArgs>debug=0</transformArgs>
                </configuration>
                <goals>
                    <goal>enhance</goal>
                </goals>
            </execution>
            <!-- enhance test code -->
            <execution>
                <id>test</id>
                <phase>process-test-classes</phase>
                <goals>
                    <goal>enhance</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
</plugins>
<!-- More elements -->
```

- Run migration. 

If we modify data structure or when we create new data structure, we can run the migration script to generate database structure definitions.

`./migration.sh`.

### 11. Add Task CRUD Resource.

`TaskResource` inherits `AbstractResource<Task>` this abstract class contains CRUD operation for `Task` Entity.

```java
package com.djammadev.resources.interview;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.avaje.ebean.Finder;
import com.djammadev.models.interview.Task;
import com.shinitech.djammadev.resources.AbstractResource;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-16
 */
@Path("/task")
@Consumes({ MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON, MediaType.MULTIPART_FORM_DATA })
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML })
public class TaskResource extends AbstractResource<Task> {

    @Override
    protected Finder<Long, Task> find() {
        return new Finder<>(Task.class);
    }
}
```

### 12. Test Application with Swagger.

- Add Swagger Annotation to Resources to be visible in Swagger UI.

`@Api("Tasks")` for `TaskResource` class

`@Api("Hello World")` for `HelloResource` class.

- Add Swagger dependency into `pom.xml`

```xml
<!-- More elements -->
<dependencies>
    <!-- More elements -->
    <dependency>
        <groupId>com.shinitech.djammadev</groupId>
        <artifactId>djamma-swagger</artifactId>
        <version>${djamma.version}</version>
    </dependency>
    <dependency>
        <groupId>com.shinitech.djammadev</groupId>
        <artifactId>djamma-swagger-ui</artifactId>
        <classifier>resources</classifier>
        <type>zip</type>
        <version>${djamma.version}</version>
    </dependency>
</dependencies>
<!-- More elements -->
<plugins>
    <plugin>
        <!-- Unzip shared resources -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
            <execution>
                <phase>package</phase>
                <goals>
                    <goal>copy</goal>
                </goals>
                <configuration>
                    <artifactItems>
                        <artifactItem>
                            <groupId>com.github.jsimone</groupId>
                            <artifactId>webapp-runner</artifactId>
                            <version>8.0.30.2</version>
                            <destFileName>webapp-runner.jar</destFileName>
                        </artifactItem>
                    </artifactItems>
                </configuration>
            </execution>
            <execution>
                <id>unpack-swagger-ui-resources</id>
                <goals>
                    <goal>unpack-dependencies</goal>
                </goals>
                <phase>generate-resources</phase>
                <configuration>
                    <outputDirectory>${project.build.directory}/${project.artifactId}</outputDirectory>
                    <includeArtifactIds>djamma-swagger-ui</includeArtifactIds>
                    <includeGroupIds>com.shinitech.djammadev</includeGroupIds>
                    <excludeTransitive>true</excludeTransitive>
                    <excludeTypes>pom</excludeTypes>
                </configuration>
            </execution>
        </executions>
    </plugin>
<!-- More elements -->
</plugins>
```

- Add swagger context into `web.xml` servelet-mapping.

```xml
<web-app>
    <!-- More elements -->
    <servlet>
        <servlet-name>Jersey2Config</servlet-name>
        <servlet-class>io.swagger.jersey.config.JerseyJaxrsConfig</servlet-class>
        <init-param>
            <param-name>api.version</param-name>
            <param-value>1.0.0</param-value>
        </init-param>
        <init-param>
            <param-name>swagger.api.basepath</param-name>
            <param-value>/api</param-value>
        </init-param>
        <load-on-startup>2</load-on-startup>
    </servlet>
</web-app>
```

- Restart and go to [http://localhost:9999/docs/v1/](http://localhost:9999/docs/v1/).

`./jersey.sh restart -b -p 9999`.

## II. Authentication Service

Now we can create a record in the database, but there is no ownership on these record.

To do so, we need to be authenticated when we want to access data resources.

### 1. Create `User` entity mapped with `T_USERS`.


```java
package com.djammadev.models.interview;

import com.shinitech.djammadev.models.ModelBase;
import com.shinitech.djammadev.models.STUser;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-20
 */
@Entity
@Table(name = "T_USERS")
public class User extends ModelBase implements STUser {
    @Column(name = "USERNAME", unique = true)
    private String username;
    @Column(name = "PASSWORD")
    private String password;

    public User() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String[] roles() {
        return new String[0];
    }
}
```

### 2. Create an Authentication Resource using `User` entity.

This resource is to simple, the logic is implemented by the framework in the class `com.shinitech.djammadev.resources.AbstractAuthenticationResource<T extends STUser>`.

`AbstractAuthenticationResource` needs a parameter inheriting from `STUser`.

```java
package com.djammadev.resources.interview;

import com.avaje.ebean.Finder;
import com.djammadev.models.interview.User;
import com.shinitech.djammadev.models.security.finder.FinderBase;
import com.shinitech.djammadev.resources.AbstractAuthenticationResource;
import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.*;

/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-20
 */
@Path("/auth")
@Api("Authentication")
@Produces({APPLICATION_JSON, TEXT_PLAIN})
@Consumes({APPLICATION_JSON, APPLICATION_FORM_URLENCODED})
public class AuthenticationResource extends AbstractAuthenticationResource<User> {
    @Override
    protected Finder<Long, User> find() {
        return new FinderBase<>(User.class);
    }
}
```

Simple right?

- Add [application.conf](src/main/resources/application.conf) file to define application properties. Authentication needs an application secret key to generate authentication token.  
After user logged in, an token is generated and returned to the client. This token can be used to communicate with the server (Bearer).  

```hocon
api {
    key="1234567890xAZERTY!"
}
```

- We can test our application to see the new Resource. 

Before we need our User table to be created.

`mvn clean install`

`./migration.sh`

- Restart and go to [http://localhost:9999/docs/v1/](http://localhost:9999/docs/v1/#/Authentication).

`./jersey.sh restart -b -p 9999`.

![Swagger](./authentication-resource.png)

### 3. Secure our resources.

We have different Authenticator filter.

* `SecuredSession`  
    - Combine session, Basic, and Bearer authentication method.
* `NeedAuthentication`  
    - Bearer or Basic authentication method.
    
We will use `SecuredSession` filter on `TaskResource`

```java
package com.djammadev.resources.interview;

/*More elements*/

@SecuredSession
public class TaskResource extends AbstractResource<Task> {

    @Override
    protected Finder<Long, Task> find() {
        return new FinderBase<>(Task.class);
    }
}
```

### 4. Add ownership on Tasks.

We have to change `AbstractResource<Task>` to `AbstractOwnerResource<Task>`, this class retrieve only the connected user elements.

```java
package com.djammadev.resources.interview;

/*More elements*/

@SecuredSession
public class TaskResource extends AbstractOwnerResource<Task> {

    @Override
    protected Finder<Long, Task> find() {
        return new FinderBase<>(Task.class);
    }

    /**
     * resource key for memcache.
     * @return key
     */
    @Override
    protected String getSubKey() {
        return "task";
    }
}
```

- Restart and go to [http://localhost:9999/docs/v1/](http://localhost:9999/docs/v1/#!/Tasks/getAll).

`./jersey.sh restart -b -p 9999`.

Before you can log in you need to create a User.

Post this data to `http://localhost:9999/api/auth/register`. on swagger [http://localhost:9999/docs/v1/#!/Authentication/register](http://localhost:9999/docs/v1/#!/Authentication/register)

```json
{
  "username": "cheickm.sissoko@djammadev.com",
  "password": "TestP4ssword"
}
```

Try to retrieve tasks you will see something like.

![Swagger](./need-authentication.png)

To login just try like bellow image

![Swagger](./login.png)

- Add more tasks on POST section of TaskResource.

/!\ If you use H2 in memory database you need to recreate all data you created each time you restart the server.

You can test the application on heroku [djammadev-tasks](https://djammadev-tasks.herokuapp.com/docs/v1)

## III. Access control

In this section we will secure our webservices with access control.  
To do that we will use Profiles and Roles.

### 1. Add Profiles and Roles Resources to the Application.

These resources are available on `DjammaRestfull` `security`'s module : `com.shinitech.djammadev.resources.security`.

Update `pom.xml` to add `security` module dependency.  

```xml
<!-- More elements -->
<dependencies>
    <!-- More elements -->
    <dependency>
        <groupId>com.shinitech.djammadev</groupId>
        <artifactId>djamma-security</artifactId>
        <version>${djamma.version}</version>
    </dependency>
    <!-- More elements -->
</dependencies>
<!-- More elements -->
```

Modify [InterviewApplication](src/main/java/com/djammadev/application/interview/InterviewApplication.java) class as :

```java
// more elements
public class InterviewApplication extends STApplicationBase {
    public InterviewApplication() {
        packages("com.djammadev.resources.interview", "com.shinitech.djammadev.resources.security");
    }
}
```

### 2. Add Profiles and Roles models to `ebean.properties` file. This allow Ebean to enhance these Entity classes.  

```hocon
# more elements
ebean.packages=com.djammadev.models.interview com.shinitech.djammadev.models.security
```

This package contains these classes.

* `Profile`  
* `ProfileRole`  => A Profile can have 0 or many Roles  
* `Role`  
* `UserRole`     => A User can have 0 or many Roles.  
* `UserProfile`  => User can have 0 or many Profiles  
* `UserRoleView` => All Roles concatenated of User.  

### 3. Add [`extra-ddl.xml`](src/main/resources/extra-ddl.xml) file to generate view and others scripts

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<extra-ddl xmlns="http://ebean-orm.github.io/xml/ns/extraddl">
    <ddl-script name="UserRoleView">
        CREATE OR REPLACE VIEW V_USER_ROLES AS
        SELECT
        R.ID AS ID,
        R.CREATE_DATE AS CREATE_DATE,
        R.MODIFY_DATE AS MODIFY_DATE,
        R.ACTIVE AS ACTIVE,
        IFNULL(U.ID, AUP.USER_ID) AS USER_ID,
        R.NAME,
        R.SUMMARY,
        R.CREATED_BY,
        R.MODIFIED_BY
        FROM T_ROLES R
        LEFT JOIN AS_PROFILE_ROLES APR ON R.ID = APR.ROLE_ID
        LEFT JOIN AS_USER_PROFILES AUP ON APR.PROFILE_ID = AUP.PROFILE_ID
        LEFT JOIN AS_USER_ROLES AUR ON AUR.ROLE_ID = R.ID
        LEFT JOIN T_USERS U ON AUP.USER_ID = U.ID OR AUR.USER_ID = U.ID;
    </ddl-script>
</extra-ddl>
```

### 4. Update `User` class to retrieve Roles.

```java
// more elements
@Entity
@Table(name = "T_USERS")
public class User extends ModelBase implements STUser {
    
    // more elements
    public List<UserRoleView> getRoles() {
        return UserRoleView.find.where().eq("userId", getId()).findList();
    }

    public List<UserProfile> getProfiles() {
        return UserProfile.find.where().eq("userId", getId()).findList();
    }

    public String[] roles() {
        List<UserRoleView> roles = getRoles();
        String[] rolesArray = new String[roles.size()];
        for (int i = 0; i < roles.size(); i++) {
            rolesArray[i] = roles.get(i).getName();
        }
        return rolesArray;
    }
}
```

Updatde `AuthentificationResource` to add roles on generated token.

```java
// more elements
public class AuthenticationResource extends AbstractAuthenticationResource<User> {

    @Override
    protected JWTCreator.Builder enrich(JWTCreator.Builder builder, User user) {
        return builder.withArrayClaim("roles", user.roles());
    }
// more elements
}
```

### 5. Generate data structure by running migration.

`mvn clean install`  
`./migration.sh`  

After migration, we got these table structures : 

* `T_ACCESS_LEVELS`  
* `T_PROFILES`  
* `AS_PROFILE_ROLES`  
* `T_ROLES`  
* `AS_USER_PROFILES`  
* `AS_USER_ROLES`  
* `V_USER_ROLES`  

You can run the Application to see the new resources.

/!\ The security resources are accessible only for `ADMIN` roles.

We can create initial data into database by using `extra-ddl.xml` file.

```xml
<!-- more elements -->
<ddl-script name="01_InsertInitialData">
    INSERT INTO T_USERS (USERNAME, PASSWORD) VALUES ('cheickm.sissoko@djammadev.com', '$2a$10$x7/Y4mv2AzX.Sh/BvbRIPexZzOj1/H9KyxSLf09cIEGyatCzRnOce');
    INSERT INTO T_PROFILES(CREATE_DATE, MODIFY_DATE, ACTIVE, NAME) VALUES (NOW(), NOW(), 1, 'ADMIN');
    INSERT INTO AS_USER_PROFILES (CREATE_DATE, MODIFY_DATE, ACTIVE, USER_ID, PROFILE_ID) VALUES (NOW(), NOW(), 1, 1, 1);
    INSERT INTO T_ROLES(CREATE_DATE, MODIFY_DATE, ACTIVE, NAME) VALUES (NOW(), NOW(), 1, 'ADMIN');
    INSERT INTO T_ROLES(CREATE_DATE, MODIFY_DATE, ACTIVE, NAME) VALUES (NOW(), NOW(), 1, 'MODERATOR');
    INSERT INTO AS_PROFILE_ROLES (CREATE_DATE, MODIFY_DATE, ACTIVE, PROFILE_ID, ROLE_ID) VALUES (NOW(), NOW(), 1, 1, 1);
</ddl-script>
```
To generate a hashed password run the command bellow :

`./migration.sh -p TestP4ssword`  
Value generated is a password hashed with `BCrypt` algorithm.

Copy-Paste into PASSWORD column value.

Re-run the migration command to generate the new extra-ddl.

`mvn clean install`  
`./migration.sh -a`  

### 6. Update TaskResource to use `AbstractSharableResource`

`AbstractSharableResource` looks like:  
```java
@SecuredSession
public abstract class AbstractSharableResource<T extends STModel> extends AbstractOwnerResource<T> {
    private static final Logger LOGGER = Logger.getLogger(AbstractSharableResource.class);

    @Override
    protected ExpressionList<T> shareFilter(STUser connected) {
        return find().select("shares").or().addAll(super.shareFilter(connected)).eq("shares.whoId", connected.getId()).endOr();
    }
}
```

`AbstractSharableResource` works as `AbstractOwnerResource` but it adds the possibility to share data with other Users.

```java
// more elements
@SecuredSession
public class TaskResource extends AbstractSharableResource<Task> {
    // more elements
}
```

### 7. Create new Entity and update `Task` entity to add share possibility on `Task`.

```java
/**
 * @author Djamma Dev by sissoko
 * @date 2019-09-29
 */
@Entity
@Table(name = "T_TASKS_SHARE", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"WHO_ID", "WHAT_ID"})
})
public class TaskShare extends ShareBase<Task> {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "ACTIVE", columnDefinition = "BIT DEFAULT TRUE")
    private Boolean active;
    @Column(name = "WHO_ID")
    private Long whoId;
    @ManyToOne
    @JoinColumn(name = "WHAT_ID", referencedColumnName = "id")
    private Task what;
    
    public TaskShare() {
        this.active = true;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public Long getWhoId() {
        return whoId;
    }

    @Override
    public void setWhoId(Long whoId) {
        this.whoId = whoId;
    }

    @Override
    @JsonIgnore
    public Task getWhat() {
        return what;
    }

    @Override
    public void setWhat(Task what) {
        this.what = what;
    }
}
```

```java
// more elements
public class Task extends ModelBase {
    // more elements
    
    @OneToMany(mappedBy = "what", cascade = CascadeType.ALL)
    private List<TaskShare> shares;
    
    // more elements
}
```

`mvn clean install`  
`./migration.sh -a`  

`./jersey.sh restart --port 9999 -b`  

[http://localhost:9999/docs/v1/](http://localhost:9999/docs/v1/).

### 8. Automatic sharing using Roles and Profiles.

Now we can update `User` entity to add a `Profile` and a `Role`. And we can use these properties to share data with a specific Role or Profile.


```java
// more elements
@JsonSerialize(using = User.Serializer.class)
public class User extends ModelBase implements STUser {
    @ManyToOne
    @JoinColumn(name = "PROFILE_ID")
    private Profile profile;
    @ManyToOne
    @JoinColumn(name = "ROLE_ID")
    private Role role;
    
    // more elements
    
    @JsonIncludeProperties({"id", "name"})
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @JsonIncludeProperties({"id", "name"})
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    // more elements
    
    static class Serializer extends STSerializer<User> {}
}
```

`STSerializer` is a Util class that is used to serialize `<T extends STMode>`.

`mvn clean install`  
`./migration.sh -a` 

### 9. Create a trigger to share record.

For this example, the trigger will share a record after insert to the Users with the same Role as the record owner.

```sql
DROP TRIGGER IF EXISTS AFTER_INSERT_TASKS_SHARE;
DELIMITER ;;
CREATE TRIGGER AFTER_INSERT_TASKS_SHARE
    AFTER INSERT
    ON T_TASKS
    FOR EACH ROW
BEGIN
    INSERT INTO T_TASKS_SHARE (WHO_ID, WHAT_ID)
    SELECT DISTINCT OTHER.ID, NEW.ID
    FROM T_USERS
             LEFT JOIN T_USERS ME ON ME.ID = NEW.OWNER_ID
             LEFT JOIN T_USERS OTHER ON ME.ROLE_ID = OTHER.ROLE_ID
    WHERE ME.ID != OTHER.ID;
END ;;
DELIMITER ;
```


`./jersey.sh restart --port 9999 -b`  

[http://localhost:9999/docs/v1/](http://localhost:9999/docs/v1/).

You can implement your own Sharing Rules.  

## IV. OAuth2

[OAuth2.md](OAuth2.md)

## V. Websocket

[Websocket.md](Websocket.md)

Thanks for reading.

